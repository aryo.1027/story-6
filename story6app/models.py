from django.db import models

# Create your models here.

class Activity(models.Model):
    activity = models.CharField(max_length=100)
    def __str__(self):
        return self.activity

class Member(models.Model):
    member = models.CharField(max_length=100)
    aktivitas = models.ForeignKey(Activity, on_delete=models.CASCADE)
    def __str__(self):
        return self.member
