from django.contrib import admin

# Register your models here.

from .models import Activity, Member

admin.site.register(Activity)
admin.site.register(Member)